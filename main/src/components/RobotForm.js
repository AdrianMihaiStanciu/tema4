import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'

class RobotForm extends React.Component{
    
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            type: '',
            mass: 0
        };
    }
    
     handleChangeName = (event) => {
        this.setState({name: event.target.value});
    }
    
     handleChangeType = (event) => {
        this.setState({type: event.target.value});
    }
    
     handleChangeMass = (event) => {
        this.setState({mass: event.target.value});
    }
    
    onAdd(event) {
    let robotnou = this.state
    RobotStore.addRobot(robotnou);
    event.preventDefault();
  }
    
    render() {

        return(
            <form onSubmit={this.onAdd}>
                 <input type="text" value={this.state.name} onChange={this.handleChangeName} />
                 <input type="text" value={this.state.type} onChange={this.handleChangeType}/>
                <input type="text" value={this.state.mass} onChange={this.handleChangeMass}/>
                 <input type="submit" value="add"/>
              </form>
        )
    }
}

export default RobotForm;